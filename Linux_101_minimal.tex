\documentclass[10pt,]{beamer}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage[]{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\PassOptionsToPackage{hyphens}{url} % url is loaded by hyperref
\usepackage{hyperref}
%\usepackage[unicode=true]{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{xcolor} % color is loaded by hyperref
\hypersetup{
            pdftitle={Linux 101 - an Audience-specific Tutorial},
            pdfauthor={Author Bryant Hansen docs@bryanthansen.net},
            colorlinks=true,
            linkcolor=green,
            citecolor=blue,
            urlcolor=blue,
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother


\title{Linux 101 - an Audience-specific Tutorial}
\author{Author Bryant Hansen docs@bryanthansen.net}
\date{May 26, 2018}

\begin{document}
\maketitle

{


% \hypersetup{linkcolor=black,pdfborderstyle={/S/U/W 1}}

\hypersetup{%
  colorlinks=true, % hyperlinks will be black
  linkbordercolor=red,% hyperlink borders will be red
  pdfborderstyle={/S/U/W 1}% border style will be underline of width 1pt
}

\setcounter{tocdepth}{3}
\tableofcontents
}
\begin{frame}[fragile]

\textbf{Document Revision History}

\begin{verbatim}
May 27, 2018
May 26, 2018
May 24, 2018
\end{verbatim}

\end{frame}

\begin{frame}[fragile]{Introduction}

\begin{block}{Original Program}

The original program is attached here, which may serve multiple
purposes.

\begin{verbatim}
    *   Man Pages
    *   Dateisystem Struktur
      *   Standard locations
      *   /proc
      *   /sys
      *   /dev
      *   /var/log
    *   OpenRC & Systemd
      *   Cron, email, services
    *   Paketverwaltungssystem (Paketmanager)
    *   Resourcen Verwaltung, Überwachung & Diagnose
    *   Core Utils
      *   Grep, sort, uniq, etc.
      *   Bash: stdin, stdout, stderr, pipes, etc.
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Original Program 2}

\begin{itemize}
\tightlist
\item
  We could use it as a starting point and follow into more detail as a
  second step.
\item
  We could skip this step and go straight into the more-detailed,
  better-structured material.
\item
  We could continue to edit, refine, and optimize the program.
\end{itemize}

What follows is a more in-depth version of the program.

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Target Audience}

The current target audience has some of the following requirements:

\begin{itemize}
\tightlist
\item
  Maintenance-focused, as opposed to deployment-focused or
  development-focused
\item
  No desktop GUI/environment
\item
  Based on Ubuntu Server (and Debian, which Ubuntu is derived from)
\end{itemize}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Latest Revision}

This content currently represents the merging of both original content
with blocks of copy-pasted content from IBM developerWorks guides
\href{https://www.ibm.com/developerworks/library/?series_title_by=learn+linux,+101}{Learn
Linux, 101}.

Editing and further customizations are currently ongoing.

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Document Format}

The native format of this document is
\href{https://wikipedia.org/wiki/Markdown}{Markdown}. It is a
lightweight markup language with plain text formatting syntax.

The document has been converted from Markdown to PDF (via Latex) using a
tool called \href{https://wikipedia.org/wiki/Pandoc}{pandoc} with the
following command:

\begin{verbatim}
  pandoc \
    -T "Linux 101" \
    --from markdown_strict+footnotes+pandoc_title_block \
    --toc \
    --template=default.latex \
    Linux_101_minimal.md \
    -o Linux_101_minimal.pdf
\end{verbatim}

It is maintained under source code control via
\href{https://wikipedia.org/wiki/Git}{git}

A remote origin can be provided if there's interest.

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Optional or Advanced Components}

These topics are still in this version of the document. We may consider
either removing these or pushing them toward the end, for whatever
possible follow-up session.

\begin{itemize}
\tightlist
\item
  Bios, Firmware, Bootloaders, and UEFI
\item
  Firewall Commands - iptables, ebtables
\item
  Encrypted Filesystems
\item
  Networked filesystems - CIFS/Samba, NFS, sshfs,
\end{itemize}

Chances are good that we don't complete what remains in a single, 1 day
session. However, at least we won't run out of topics of discussion.

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Removed Components}

A set of topics was removed in the latest ``minimal'' revision.

\begin{itemize}
\tightlist
\item
  xargs
\item
  Use basic SQL commands
\item
  Install a boot manager
\item
  Manage shared libraries
\item
  Process execution priorities
\item
  Manage disk quotas
\item
  Network analysis tools
\end{itemize}

The topics did not make the short list for whatever reason. They may be
considered later if more-advanced training is required.

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Training Format}

This training material is intended to be used in the following setting:

\begin{itemize}
\tightlist
\item
  A group environment
\item
  With both contributors and trainees with various IT-related
  backgrounds
\item
  With everyone having the equipment to experiment on-the-fly at any
  point in time
\end{itemize}

\end{block}

\end{frame}

\begin{frame}{System architecture}

\begin{block}{Typical Hardware View}

\end{block}

\begin{block}{\includegraphics{./top_view1c.png} {[}2{]}}

\end{block}

\begin{block}{\includegraphics{./front_view1.png} {[}2{]}}

\end{block}

\begin{block}{\includegraphics{rear_view1.png} {[}2{]}}

\end{block}

\begin{block}{Physical Connectors, Connections, and Indicators}

\begin{block}{Blinky Lights and Indicators}

A review of the LED's on the device and the meaning of each.

\begin{itemize}
\tightlist
\item
  Power
\item
  Drive Indicators
\item
  Network Activity
\end{itemize}

\end{block}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{External I/O: plugs, buttons, sockets, wireless}

\begin{itemize}
\tightlist
\item
  Power Input - redundant?
\item
  Power \& Reset buttons
\item
  Display - DP2, DPI, HDMI, DVI, VGA
\item
  USB2, USB3, USB-C
\item
  Network - CAT-5, CAT-6
\item
  Wifi
\item
  BlueTooth - BLE
\item
  IR
\item
  RS-232
\item
  Other?
\end{itemize}

\end{block}

\begin{block}{Internal Connections}

\begin{itemize}
\tightlist
\item
  SATA
\item
  PCI Bus
\item
  I2C Bus
\item
  Power
\item
  Fans
\end{itemize}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{System Resource Management}

The initial version of this document was very resource-focused. In
subsequent revisions, portions of other training materials were
incorporated, and this order evolved somewhat.

The following show an overview of commands that one would typically use
to audit specific system resouces.

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Physical Resources}

\begin{verbatim}
  * CPU - ps, top, htop, /proc/cpuinfo
  * RAM - /proc/meminfo, free, swapon, tmpfs
  * Storage - hdparm, du, df, iotop
  * RAID Controller (platform-specific)
\end{verbatim}

\end{block}

\begin{block}{Bus Connections}

\begin{verbatim}
  * PCI - lspci
  * USB - lsusb
  * Network - ifconfig, route, ntop
  * Wifi - iwconfig, wpa_supplicant, iwlist
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Platform Components}

\begin{itemize}
\tightlist
\item
  kernel -- make menuconfig, make oldconfig, /sys, /proc
\item
  modules -- lsmod, modinfo, modprobe,
\item
  device nodes -- /dev, /sys
\item
  files \& directories -- permissions, umask,
\item
  processes
\end{itemize}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Bios, Firmware, Bootloaders, and UEFI}

Guide the system through the booting process. Tasks include giving
common boot loader commands and kernel options at boot time, knowing the
boot sequence from BIOS to boot completion, understanding the
traditional SysVinit initialization alternatives and newer alternatives,
and checking boot events in the log files. {[}1{]}

\begin{itemize}
\tightlist
\item
  EFI/UEFI
\item
  Bios, Integrated Lights Out, RAID
\end{itemize}

\emph{Bootloaders:}

\begin{itemize}
\tightlist
\item
  grub 0.97, grub 2.0
\item
  syslinux/isolinux
\item
  Refind/Refit
\end{itemize}

\emph{Kernel parameters and options}

\begin{verbatim}
  * /proc/cmdline
  * /usr/src/linux/Documentation/
  * /etc/modules*
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}[fragile]{Getting Started}

\begin{block}{Boot and Login}

A login may require an SSH connection to the PC. On the text console,
the interface is nearly identical.

\begin{verbatim}
  * whoami, root, users, groups, su, sudo
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Basics of the Linux command line and the bash shell}

Interact with shells and commands using the command line. This task
includes typing valid commands and command sequences; defining,
referencing and exporting environment variables; using command history
and editing facilities; invoking commands in the path and outside the
path; and using man pages to find out about commands. {[}1{]}

\begin{itemize}
\tightlist
\item
  man pages
\item
  useful resources: -- \href{http://tldp.org/LDP/abs/html/}{Advanced
  Bash Scripting Guide} --
  \href{http://www.faqs.org/docs/artu/ch01s06.html}{Basics of the Unix
  Philosophy}
\end{itemize}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{File editing in the console environment}

\emph{Editors:}

\begin{itemize}
\tightlist
\item
  vi
\item
  nano
\item
  less
\end{itemize}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Customize and use the shell environment}

Meet your user needs. Tasks include customizing shell environments to
meet user needs; modifying global and user profiles; setting environment
variables, such as PATH, at login or when spawning a new shell; writing
Bash functions for frequently used sequences of commands; maintaining
skeleton directories for new user accounts; and setting command search
paths with the proper directories. {[}1{]}

\begin{verbatim}
  * pwd, ~
  * env - PS1, SHELL, 
  * export
  * alias
  * source
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Advanced Examples}

Contents of /etc/bash/bashrc

This is /etc/bash.bashrc on Ubuntu

\textbf{Shell Prompt (PS1)}

\begin{verbatim}
  if ${use_color} ; then
      NORMAL="\[\e[0m\]"
      RED="\[\e[1;31m\]"
      GREEN="\[\e[1;32m\]"
      BLUE="\[\033[1;34m\]"
      MAGENTA="\[\033[1;35m\]"

      DT='date "+[%Y-%m-%d %H:%M:%S.%N] %Z WK%W %A"'
      if [[ ${EUID} == 0 ]] ; then
        PS1="\n${MAGENTA}\$(${DT})\n${BLUE}\w\n${RED}\h -> ${NORMAL}"
      else
        PS1="\n${MAGENTA}\$(${DT})\n${BLUE}\w\n${GREEN}\u@\h${BLUE} -> ${NORMAL}"
      fi
  else
      if [[ ${EUID} == 0 ]] ; then
          # show root@ when we don't have colors
          PS1+='\u@\h \w \$ '
      else
          PS1+='\u@\h \w \$ '
      fi
  fi
\end{verbatim}

Result:

\begin{verbatim}
  [2018-05-27 15:25:49.876892051] CEST WK21 Sonntag
  ~
  slice ->
\end{verbatim}

\includegraphics{./prompt1.png}

\end{block}

\end{frame}

\begin{frame}[fragile]{Devices, Linux filesystems, Filesystem Hierarchy
Standard}

\begin{block}{Drivers \& Kernel Modules}

\begin{verbatim}
  * lsmod, modinfo, modprobe, /proc/config.gz
  * initrd, mkinitrd
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Partitions and Filesystems}

Configure disk partitions and create filesystems or swap space on media
such as hard disks, and design a disk partitioning scheme for a Linux
system. Tasks include understanding the different types of common Linux
filesystems and allocating filesystems and swap space to separate
partitions or disks. {[}1{]}

\begin{block}{Partitions}

\begin{verbatim}
  tools - fdisk, parted, gparted, cfdisk, dd
  configuration - /etc/fstab
  swap - swap files, swapon, free, /proc/meminfo
  tmpfs - RAMDISK
  capabilities - /proc/filesystems
  status - blkid, /etc/mtab, mount -l
  sync
  lsof - can it be unmounted?
\end{verbatim}

\end{block}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Root Filesystem / Filesystem Hierarchy Standard}

Understand the Filesystem Hierarchy Standard (FHS), including typical
file locations and directory classifications. Tasks include
understanding the correct locations of files under the FHS, finding
files and commands on a Linux system, and learning the location and
purpose of important file and directories as defined in the FHS. {[}1{]}

\begin{verbatim}
  /SWAP_16G.img
  /bin
  /boot
  /dev
  /etc
  /home
  /lib
  /lib32
  /lib64
  /lost+found
  /media
  /mnt
  /opt
  /proc
  /root
  /run
  /sbin
  /sys
  /tmp
  /usr
  /var
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Filesystem Types}

\textbf{A non-comprehensive list:}

\begin{itemize}
\tightlist
\item
  ext2, ext3, ext4
\item
  xfs
\item
  iso9660
\item
  ntfs, vfat, msdos
\item
  hfs, hfsplus
\item
  fuse
\item
  nfs, nfs4, ntfs
\item
  sysfs
\item
  ramfs, tmpfs
\item
  autofs
\item
  overlay
\item
  btrfs
\item
  zfs
\end{itemize}

\textbf{General Categories:}

\begin{itemize}
\tightlist
\item
  Linux-oriented
\item
  Windows-oriented
\item
  OSX-oriented
\item
  Read-Only Media (platform-independent)
\item
  Filesystem-in-Userspace Adaption Layer
\item
  Overlay
\item
  Next-generation
\item
  Special Notes about zfs -- data deduplication, snapshots, RAID-Z,
  mirrors \& pools
\end{itemize}

see \url{https://wikipedia.org/wiki/Comparison_of_file_systems}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{File and directory management}

Use the basic UNIX commands to copy, move, and remove files and
directories. Tasks include advanced file management operations such as
copying multiple files recursively, removing directories recursively,
using wildcard patterns, finding files and acting on them based on type
size or time, and using tar, cpio, and dd commands. {[}1{]}

\begin{verbatim}
  * find, ls, stat, pushd, popd, cd, mkdir, rm, rmdir, touch, 
\end{verbatim}

\textbf{Advanced Example:}

Find files in a particular subdirectory with the same md5sums

\begin{verbatim}
  find ./mydir -type f -exec md5sum "{}" \; | sort | uniq -d -w 32
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Maintain the integrity of filesystems}

Maintain a standard filesystem or journaling filesystem. Tasks include
verifying the integrity of filesystems, monitoring free space and
inodes, and repairing simple filesystem problems. {[}1{]}

\begin{verbatim}
  * dmesg, df, df -h, ls -i, stat
  * fsck.ext4, fsck.fat, fsck.hfs, fsck.hfsplus, fsck.xfs, fsck.cramfs
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Control mounting and unmounting of filesystems}

Configure the mounting of a filesystem. Tasks include manually mounting
and unmounting filesystems, configuring filesystem mounting on bootup,
and configuring user-mountable removable filesystems. {[}1{]}

\begin{verbatim}
  * mount, umount, dmesg, /dev/sd*, 
\end{verbatim}

\textbf{Example:}

This command can be used to monitor for hotplub events in a 2-second
polling loop

\begin{verbatim}
  watch 'dmesg | tail -n 40'
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Manage file permissions and ownership}

Control file access through the proper use of permissions and
ownerships. Tasks include managing access permissions on regular and
special files as well as directories; using access modes such as suid,
sgid, and the sticky bit to maintain security; learning how to change
the file creation mask; and using the group field to grant file access
to group members. {[}1{]}

\begin{verbatim}
  * chown, chmod, ls -al, stat, touch
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Manage user and group accounts and related system files}

Add, remove, suspend, and change user accounts. Tasks include adding,
modifying, and removing users and groups; managing user and group
information in password and group databases; and creating and managing
special-purpose and limited accounts. {[}1{]}

\begin{verbatim}
  * useradd, usermod, groups, chgrp
  * /etc/skel
  * /etc/passwd, /etc/group, /etc/shadow
  * passwd
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Create and change hard and symbolic links}

Create and manage hard and symbolic links to a file. Tasks include
creating links, identifying hard and or soft links, understanding the
difference between copying and linking files, and using links to support
system administration tasks. {[}1{]}

\begin{verbatim}
  * ln, ln -s, ls -i
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}[fragile]{System Configuration}

\begin{block}{Hardware and Platform}

Determine and configure fundamental system hardware. This task covers
integrated and external peripherals, coldplug and hotplug devices, and
different kinds of mass storage devices. Tools related to devices,
including USB devices, setting IDs, especially for booting, and
low-level facilities such as sysfs, udev, and dbus are also covered.
{[}1{]}

\begin{verbatim}
  * /dev/ -- device tree
  * /etc/inittab
  * udev, systemctl, /etc/init.d/, /etc/systemd/, dmesg
  * lspci, lsusb
  * /proc/net/dev
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Power Management}

\begin{verbatim}
  * /proc/cpuinfo
  * battery monitoring
  * pm-suspend, pm-hybernate
  * screen brightness
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Runlevels, boot targets, shutdown, and reboot}

Manage the runlevel of the system. Tasks include changing to single-user
mode, shutting down or rebooting the system, setting the default
runlevel, switching between runlevels, alerting users, and properly
terminating process. {[}1{]}

\begin{itemize}
\tightlist
\item
  OpenRC
\item
  Systemd
\item
  System Services
\end{itemize}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{System logging}

Configure the syslog daemon and the logging daemon to send log output to
a central log server or accept log output as a central log server. Tasks
include understanding syslog configuration files, the syslog facility,
and standard facilities, priorities, and actions. {[}1{]}

\begin{verbatim}
  * /var/log, /var/log/messages
  * log monitoring: tail -f
  * /etc/syslog/*.conf
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Localization and internationalization}

Localize a system in a language other than English and understand why
\textbar{}LANG=C\textbar{} is useful when scripting. Tasks include
understanding locale and timezone settings. {[}1{]}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Maintain system time (NTP)}

Maintain the system time and synchronize the clock via NTP. Tasks
include setting the system date and time, setting the hardware clock to
the correct time in UTC, configuring the timezone, configuring NTP, and
understanding the pool.ntp.org service. {[}1{]}

\begin{verbatim}
  * ntp, ntpd, ntpdate
  * port 123
  * servers & clients - Windows & Linux (Win7 needed regedit for server)
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Automate system administration tasks by scheduling jobs}

\begin{itemize}
\tightlist
\item
  Monitoring
\item
  Alarms - NewRelic, Slack,
\item
  Synchronization and Backups - scp, rsync, ssh, netcat,
\end{itemize}

Maintain a standard filesystem or journaling filesystem. Tasks include
using the cron or anacron facilities to run jobs at regular intervals
and the \textbar{}at\textbar{} command to run jobs at a specific time.
{[}1{]}

\begin{itemize}
\tightlist
\item
  cron
\item
  atd
\item
  incrond
\item
  systemd - system-ctl
\end{itemize}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Debian/Ubuntu package management}

Perform package management using the Debian package tools. Tasks include
installing, upgrading, and removing Debian binary packages, finding
packages containing specific files or libraries, and obtaining package
information like version, content, dependencies, package integrity, and
installation status. {[}1{]}

\emph{Commands:}

\begin{verbatim}
  * apt-get
  * apt-cache
  * dpkg
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}{Create, monitor, and kill processes}

Manage processes. This task includes knowing how to run jobs in the
foreground and background, bringing a job from the background to the
foreground and vice versa, starting a process that will run without
being connected to a terminal, and signaling a program to continue
running after logout. Tasks also include monitoring active processes,
selecting and sorting processes for display, sending signals to
processes, and killing processes. {[}1{]}

\begin{itemize}
\tightlist
\item
  background commands, \&, fg, bg, ctrl-Z, nohup, jobs, kill, signal
\item
  analyze per-process resource usage: CPU, Memory, Drive I/O, Network
  I/O
\item
  ps, top, htop, iotop, ntop, netstat, ifconfig
\end{itemize}

\end{frame}

\begin{frame}{Bash Shell, Scripting, and Data Management}

\begin{block}{Streams, pipes, and redirects}

Redirect streams and connect them to efficiently process textual data.
Tasks include redirecting standard input, standard output, and standard
error; piping the output of one command to the input of another command;
using the output of one command as arguments to another command; and
sending output to both stdout and a file. {[}1{]}

\begin{itemize}
\tightlist
\item
  stdin, stdout, stderr
\item
  /proc/NNN/fd/N, /proc/NNN/fdinfo/N
\item
  The beauty and elegance of the pipe: \textbf{\textbar{}}
\end{itemize}

Example: Determine the progress of a very log file I/O operation,
performed via dd

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Text streams and filters}

Apply filters to text streams. This task includes sending text files and
output streams through text utility filters to modify the output {[}1{]}

\begin{verbatim}
  * cat, head, tail, dd
  * sort, uniq
  * tr
  * tee
  * wc -c, wc -l
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Search text files using regular expressions}

Manipulate files and text data using regular expressions. Tasks include
creating simple regular expressions that contain several notational
elements and use regular expression tools to perform searches through a
filesystem or file content. {[}1{]}

\begin{verbatim}
  * grep: -A -B -n -r -i
  * sed: file, stdin, inline, case
  * Advanced: perl and python
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Customize or write simple scripts}

Customize existing Bash scripts, or write simple new ones. Tasks include
using standard shell loop and test syntax; using command substitution;
testing return values for success or failure; conditionally mailing the
superuser, selecting the correct script interpreter through the shebang
(\#!) line; and managing the location, ownership, execution, and
suid-rights of scripts. {[}1{]}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Advanced Scripts}

This may be beyond the level of this course, but they should have
interesting functionality in a few-line copy-paste script into a bash
shell prompt.

\begin{block}{Find duplicate files in an arbitrary set of directory
trees}

\begin{verbatim}
  time \
  find . \
    -type f \
    -exec md5sum "{}" \; \
    | sort \
    | uniq --check-chars=32 --all-repeated \
    | while read md5 filename ; do
        echo -n "$md5 "
        stat --format="%i %s %n" "$filename"
    done \
    | uniq --check-chars=32 --group=append
\end{verbatim}

Note: the specified directory is simply .

This means search in the current directory and every subdirectory under
it

The sequence of events is as follows:

\begin{itemize}
\tightlist
\item
  find all files in the current directory
\item
  perform an MD5 checksum of the file
\item
  sort the results by md5 and filter only duplicate values
\item
  get the inode and file size of each duplicate
\item
  add a blank line between blocks of duplicates for easier viewing
\end{itemize}

At this point, we can either delete our duplicates or replace them with
hard links, freeing up the drive resources

\end{block}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Alternative with xargs}

This version performs multiple checksum operations in parallel,
significantly-increasing performance.

\begin{verbatim}
  time \
  find . \
    -type f \
    | xargs -n1 -d '\n' -P4 md5sum \
    | sort \
    | uniq --check-chars=32 --all-repeated \
    | while read md5 filename ; do
        echo -n "$md5 "
        stat --format="%i %s %n" "$filename"
    done \
    | uniq --check-chars=32 --group=append
\end{verbatim}

The next level of optimization would be to perform the checksum only 1
time per inode. Suggestions on how best to do that?

\end{block}

\end{frame}

\begin{frame}{Networking fundamentals}

\begin{block}{Fundamentals of internet protocols}

Understand TCP/IP network fundamentals. Tasks include learning about
network masks; knowing the differences between private and public dotted
decimal IP addresses; setting a default route; understanding common TCP
and UDP ports (20, 21, 22, 23, 25, 53, 80, 110, 119, 139, 143, 161, 443,
465, 993, and 995); understanding the differences and major features of
UDP, TCP, and ICMP; and knowing the major differences between IPv4 and
IPv6. {[}1{]}

\begin{itemize}
\tightlist
\item
  Topics: servers, sockets
\item
  Commands: ping, traceroute, ntop
\end{itemize}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Basic network configuration}

View, change, and verify configuration settings on client hosts. Tasks
include manually and automatically configuring network interfaces and
configuring TCP/IP hosts. {[}1{]}

\begin{verbatim}
  * udev, systemd-networkd
  * /etc/networking/interfaces
  * /proc/net/dev
  * ifconfig
  * WPA Supplicant <https://w1.fi/wpa_supplicant/>
     -- /etc/wpa_supplicant/wpa_supplicant.conf
  * iwconfig, iwlist
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Examples}

\begin{verbatim}
  * watch 'iwconfig ; ifconfig'

  * giving a USB-to-Ethernet (or Wifi) dongle a specific name
    and a static IP address on hotplug
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Basic network troubleshooting}

Troubleshoot networking issues on client hosts. Tasks include manually
and automatically configuring network interfaces and routing tables to
add, start, stop, restart, delete, or reconfigure network interfaces;
changing, viewing, or configuring the routing table; correcting an
improperly set default route manually; and debugging problems associated
with the network configuration. {[}1{]}

\textbf{Commands:}

\begin{verbatim}
  * ping, traceroute
  * route
  * netstat, tcpdump
  * netcat
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}[fragile]

\begin{block}{Examples}

\begin{verbatim}
  * netstat -anp | grep LIST
  * tcpdump -i eth0 -n not port 22
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Configure client side DNS}

Configure DNS on a client host. Tasks include using DNS on the local
system and modifying the order in which name resolution is done. {[}1{]}

\begin{itemize}
\tightlist
\item
  /etc/nsswitch.conf
\item
  /etc/resolv.conf
\item
  /etc/systemd/network/
\item
  Avahi, Bonjour, Link-local addressing
\item
  nslookup, whois
\end{itemize}

\end{block}

\end{frame}

\begin{frame}{Security}

\begin{block}{Perform security administration tasks}

\begin{itemize}
\tightlist
\item
  Review system configuration to ensure host security in accordance with
  local security policies
\item
  Find files with the suid/sgid bit set
\item
  Setting or changing user passwords and password aging information
\item
  netstat to discover open ports on a system
\item
  setting limits on user logins
\item
  basic sudo configuration and usage
\end{itemize}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Firewalls}

\begin{itemize}
\tightlist
\item
  iptables, ebtables
\item
  shadow passwords and how they work
\item
  stopping network services not in use
\item
  TCP wrappers
\end{itemize}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{OpenSSH}

\begin{itemize}
\tightlist
\item
  \textasciitilde{}/.ssh/config
\item
  Private/Public Key - authorized\_keys
\item
  port forwarding and tunneling
\item
  SOCKS proxy
\item
  X11 tunnels
\end{itemize}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Securing data with encryption}

\begin{itemize}
\tightlist
\item
  symmetric vs.~assymetric encryption
\item
  key management
\item
  encrypted filesystems
\item
  GnuPG
\end{itemize}

\end{block}

\end{frame}

\begin{frame}[fragile]{Useful Examples}

\textbf{Total size of a list of subdirectories that are 1GB or greater}

\begin{verbatim}
du -sh /data/media/pictures/* /data/os/virtualbox/* | grep "^[0-9]*G"
\end{verbatim}

\end{frame}

\begin{frame}[fragile]

\begin{block}{ADB incremental sync}

Base command:

\begin{verbatim}
adb shell ls -al /mnt/sdcard/DCIM/Camera/

adb pull /mnt/sdcard/DCIM/Camera/<some image>.JPG ./
\end{verbatim}

Automated recursion:

\begin{verbatim}
adb shell 'ls /mnt/sdcard/DCIM/Camera/' \
| tr -d 'r' \
| while read f ; do
    b="$(basename "$f")"
    [[ ! -f "$b" ]] \
    && echo adb pull /mnt/sdcard/DCIM/Camera/$f ./
done 2>&1 \
| tail -n 2
\end{verbatim}

\end{block}

\end{frame}

\begin{frame}{References \{.allowframebreaks\}}

\begin{block}{{[}1{]} LPIC-1: Linux Server Professional Certification}

This series of tutorials helps you learn Linux system administration
tasks. The topics mirror those of the Linux Professional Institute's
\url{http://www.lpi.org/} LPIC-1: Linux Server Professional
Certification exams. You can use the tutorials to prepare for
certification, or you can use them to learn about Linux.

The material in these tutorials corresponds to version 4.0 of the
objectives of the LPIC-1 exams as updated April 15th, 2015 for LPIC-1

LPIC-1 exams:

\begin{itemize}
\tightlist
\item
  101
  \url{http://www.lpi.org/study-resources/lpic-1-101-exam-objectives/}
\item
  102
  \url{http://www.lpi.org/study-resources/lpic-1-102-exam-objectives/}
\item
  Directory of Study Topics:
  \url{https://www.ibm.com/developerworks/library/?series_title_by=learn+linux,+101}
\end{itemize}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{IBM developerWorks}

A subset of developerWorks links included in the original document

\begin{itemize}
\tightlist
\item
  YouTube \url{https://www.youtube.com/user/developerworks}
\item
  Tutorials \& training \url{https://www.ibm.com/developerworks/learn/}
\item
  Demos \& sample code \url{https://developer.ibm.com/accelerate/}
\item
  Q\&A forums \url{https://developer.ibm.com/answers}
\item
  dW Blog \url{https://developer.ibm.com/dwblog/}
\item
  Events \url{https://developer.ibm.com/events/}
\item
  Courses \url{https://developer.ibm.com/courses/}
\item
  Learn \url{https://www.ibm.com/developerworks/learn/}
\item
  Linux \url{https://www.ibm.com/developerworks/linux/}
\end{itemize}

\end{block}

\end{frame}

\begin{frame}

\begin{block}{Miscellaneous}

\begin{itemize}
\item
  \href{http://tldp.org/LDP/abs/html/}{Advanced Bash Scripting Guide}
\item
  {[}2{]} {[}HP Proliant DL385
  Produkt{]}(https://www.hpe.com/at/de/product-catalog/servers/proliant-servers/pip.hpe-proliant-dl385-gen10-server.1010268408.html)
\end{itemize}

\end{block}

\end{frame}

\begin{frame}{Merci Vielmals!}

\end{frame}

\begin{frame}

\end{frame}

\end{document}
