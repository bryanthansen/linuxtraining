% Linux 101 - an Audience-specific Tutorial
% Author Bryant Hansen docs@bryanthansen.net
% May 26, 2018


# Introduction

This content currently represents the merging of both original content with
blocks of copy-pasted content from IBM developerWorks guides
[Learn Linux, 101](https://www.ibm.com/developerworks/library/?series_title_by=learn+linux,+101).

Editing and further customizations are currently ongoing.

The current target audience has some of the following requirements:

* Maintenance-focused, as opposed to deployment-focused or development-focused
* No desktop GUI/environment
* Based on Ubuntu Server

This document has been converted to PDF
via [pandoc](https://en.wikipedia.org/wiki/Pandoc)
with the following command:

    pandoc --template=default.latex Linux_101.md -o Linux_101.pdf

It is maintained under source code control
via [git](https://en.wikipedia.org/wiki/Git)


**Document Revision History**

    May 24, 2018
    May 24, 2018

## Original Program

The original program is attached here, which may serve multiple purposes.

        *   Man Pages
        *   Dateisystem Struktur
          *   Standard locations
          *   /proc
          *   /sys
          *   /dev
          *   /var/log
        *   OpenRC & Systemd
          *   Cron, email, services
        *   Paketverwaltungssystem (Paketmanager)
        *   Resourcen Verwaltung, Überwachung & Diagnose
        *   Core Utils
          *   Grep, sort, uniq, etc.
          *   Bash: stdin, stdout, stderr, pipes, etc.

We could use it as a starting point and follow into more detail as a second
step.  We could skip this step and go straight into the more-detailed,
better-structured material.  Or we could continue to edit, refine, and
optimize the program.


# System architecture

## Typical Hardware View

![Top View](./top_view1c.png)
![Front View](./front_view1.png)
![Rear View](rear_view1.png)

## Physical Connectors, Connections, and Indicators

### Blinky Lights and Indicators

A review of the LED's on the device and the meaning of each.

* Power
* Drive Indicators
* Network Activity

### External

* Power Input - redundant?
* Power & Reset buttons
* Display - DP2, DPI, HDMI, DVI, VGA
* USB2, USB3, USB-C
* Network - CAT-5, CAT-6
* Wifi
* BlueTooth - BLE
* IR
* RS-232
* Other?

### Internal

* PCI Bus
* I2C Bus
* Sata-III
* Fans

## System Resources Management

The following show an overview of commands that one would typically use
to audit specific system resouces.

### Physical Resources

        * CPU - ps, top, htop, /proc/cpuinfo
        * RAM - /proc/meminfo, free, swapon,
        * Storage - hdparm, du, df, iotop
        * RAID Controller (platform-specific)

### Bus Connections

        * PCI - lspci
        * USB - lsusb
        * Network - ifconfig, route, ntop
        * Wifi - iwconfig, wpa_supplicant

## Platform Components

* kernel
   -- make menuconfig, make oldconfig, /sys, /proc
* modules
   -- lsmod, modinfo, modprobe,
* device nodes
   -- /dev, /sys
* files & directories
   -- permissions, umask,
* processes

## Bios, Firmware, Bootloaders, and UEFI

Guide the system through the booting process. Tasks include giving
common boot loader commands and kernel options at boot time, knowing the
boot sequence from BIOS to boot completion, understanding the
traditional SysVinit initialization alternatives and newer alternatives,
and checking boot events in the log files.

* EFI/UEFI
* Bios, Integrated Lights Out, RAID

*Bootloaders:*

* grub 0.97, grub 2.0
* syslinux/isolinux
* Refind/Refit
* lilo

*Kernel parameters and options*

* /proc/cmdline

# Getting Started

## Boot and Login

A login may require an SSH connection to the PC.  On the text console,
the interface is nearly identical.

* whoami, root, users, groups, su, sudo

## Basics of the Linux command line and the bash shell

Interact with shells and commands using the command line. This task
includes typing valid commands and command sequences; defining,
referencing and exporting environment variables; using command history
and editing facilities; invoking commands in the path and outside the
path; and using man pages to find out about commands.

        * man pages
        * useful resources:
          -- [Advanced Bash Scripting Guide](http://tldp.org/LDP/abs/html/)
          -- [Basics of the Unix Philosophy](http://www.faqs.org/docs/artu/ch01s06.html)

## File editing in the console environment

*Editors:*

*  vi
*  nano
*  less

## Customize and use the shell environment

Meet your user needs. Tasks include customizing shell environments to
meet user needs; modifying global and user profiles; setting environment
variables, such as PATH, at login or when spawning a new shell; writing
Bash functions for frequently used sequences of commands; maintaining
skeleton directories for new user accounts; and setting command search
paths with the proper directories.

        * pwd, ~
        * env - PS1, SHELL, 
        * export
        * alias
        * source


# Devices, Linux filesystems, Filesystem Hierarchy Standard

## Drivers & Kernel Modules

        * lsmod, modinfo, modprobe, /proc/config.gz
        * initrd, mkinitrd

## Partitions and Filesystems

Configure disk partitions and create filesystems or swap space on media
such as hard disks, and design a disk partitioning scheme for a Linux
system. Tasks include understanding the different types of common Linux
filesystems and allocating filesystems and swap space to separate
partitions or disks.

### Partitions

        tools - fdisk, parted, gparted, cfdisk, dd
        swap - swap files, swapon, free, /proc/meminfo
        tmpfs - RAMDISK
        /proc/filesystems

### Root Filesystem / Filesystem Hierarchy Standard

Understand the Filesystem Hierarchy Standard (FHS), including typical
file locations and directory classifications. Tasks include
understanding the correct locations of files under the FHS, finding
files and commands on a Linux system, and learning the location and
purpose of important file and directories as defined in the FHS.

        /SWAP_16G.img
        /bin
        /boot
        /dev
        /etc
        /home
        /lib
        /lib32
        /lib64
        /lost+found
        /media
        /mnt
        /opt
        /proc
        /root
        /run
        /sbin
        /sys
        /tmp
        /usr
        /var

## File and directory management

Use the basic UNIX commands to copy, move, and remove files and
directories. Tasks include advanced file management operations such as
copying multiple files recursively, removing directories recursively,
using wildcard patterns, finding files and acting on them based on type
size or time, and using tar, cpio, and dd commands.

Commands:

        find
        ls
        stat

## Maintain the integrity of filesystems

Maintain a standard filesystem or journaling filesystem. Tasks include
verifying the integrity of filesystems, monitoring free space and
inodes, and repairing simple filesystem problems.

        * e2fsck, dmesg

## Control mounting and unmounting of filesystems

Configure the mounting of a filesystem. Tasks include manually mounting
and unmounting filesystems, configuring filesystem mounting on bootup,
and configuring user-mountable removable filesystems.

        * mount, umount, dmesg, /dev/sd*, 

## Manage file permissions and ownership

Control file access through the proper use of permissions and
ownerships. Tasks include managing access permissions on regular and
special files as well as directories; using access modes such as suid,
sgid, and the sticky bit to maintain security; learning how to change
the file creation mask; and using the group field to grant file access
to group members.

        * chown, chmod, ls -al, stat, touch

## Manage user and group accounts and related system files

Add, remove, suspend, and change user accounts. Tasks include adding,
modifying, and removing users and groups; managing user and group
information in password and group databases; and creating and managing
special-purpose and limited accounts.

        * useradd, usermod, groups, chgrp
        * /etc/skel
        * /etc/passwd, /etc/group, /etc/shadow
        * passwd

## Create and change hard and symbolic links

Create and manage hard and symbolic links to a file. Tasks include
creating links, identifying hard and or soft links, understanding the
difference between copying and linking files, and using links to support
system administration tasks.

        * ln, ln -s, ls -i


# System Configuration

## Hardware and Platform

Determine and configure fundamental system hardware. This task covers
integrated and external peripherals, coldplug and hotplug devices, and
different kinds of mass storage devices. Tools related to devices,
including USB devices, setting IDs, especially for booting, and
low-level facilities such as sysfs, udev, and dbus are also covered.

        * /dev/ device tree
        * /etc/inittab
        * udev, systemctl, /etc/init.d/, /etc/systemd/, dmesg
        * lspci, lsusb
        * /proc/net/dev

### Power Management

        * /proc/cpuinfo
        * battery monitoring
        * pm-suspend, pm-hybernate
        * screen brightness

## Runlevels, boot targets, shutdown, and reboot

Manage the runlevel of the system. Tasks include changing to single-user
mode, shutting down or rebooting the system, setting the default
runlevel, switching between runlevels, alerting users, and properly
terminating process.

* OpenRC
* Systemd

## System logging

Configure the syslog daemon and the logging daemon to send log output to
a central log server or accept log output as a central log server. Tasks
include understanding syslog configuration files, the syslog facility,
and standard facilities, priorities, and actions.

        * /var/log, /var/log/messages
        * log monitoring: tail -f
        * /etc/syslog/*.conf

## Localization and internationalization

Localize a system in a language other than English and understand why
|LANG=C| is useful when scripting. Tasks include understanding locale
and timezone settings.

## Maintain system time

Maintain the system time and synchronize the clock via NTP. Tasks
include setting the system date and time, setting the hardware clock to
the correct time in UTC, configuring the timezone, configuring NTP, and
understanding the pool.ntp.org service.

        * ntp, ntpd, ntpdate

## Automate system administration tasks by scheduling jobs

* Monitoring
* Alarms
* Synchronization and Backups

Maintain a standard filesystem or journaling filesystem. Tasks include
using the cron or anacron facilities to run jobs at regular intervals
and the |at| command to run jobs at a specific time.

* cron
* atd
* incrond
* systemd

## Debian/Ubuntu package management

Perform package management using the Debian package tools. Tasks include
installing, upgrading, and removing Debian binary packages, finding
packages containing specific files or libraries, and obtaining package
information like version, content, dependencies, package integrity, and
installation status.

*Commands:*

        * apt-get
        * apt-cache
        * dpkg


# Create, monitor, and kill processes

Manage processes. This task includes knowing how to run jobs in the
foreground and background, bringing a job from the background to the
foreground and vice versa, starting a process that will run without
being connected to a terminal, and signaling a program to continue
running after logout. Tasks also include monitoring active processes,
selecting and sorting processes for display, sending signals to
processes, and killing processes.

* background commands, &, fg, bg, ctrl-Z, nohup, jobs, kill, signal
* analyzing resource usage: CPU, Memory, Drive I/O, Network I/O


# Bash Shell, Scripting, and Data Management

## Streams, pipes, and redirects

Redirect streams and connect them to efficiently process textual data.
Tasks include redirecting standard input, standard output, and standard
error; piping the output of one command to the input of another command;
using the output of one command as arguments to another command; and
sending output to both stdout and a file.

* stdin, stdout, stderr

## Text streams and filters

Apply filters to text streams. This task includes sending text files and
output streams through text utility filters to modify the output

        * cat, head, tail, dd
        * tr
        * tee

### Search text files using regular expressions

Manipulate files and text data using regular expressions. Tasks include
creating simple regular expressions that contain several notational
elements and use regular expression tools to perform searches through a
filesystem or file content.

        * grep: -A -B -n -r -i
        * sed: file, stdin, inline, case
        * Advanced: perl and python

## Customize or write simple scripts

Customize existing Bash scripts, or write simple new ones. Tasks include
using standard shell loop and test syntax; using command substitution;
testing return values for success or failure; conditionally mailing the
superuser, selecting the correct script interpreter through the shebang
(#!) line; and managing the location, ownership, execution, and
suid-rights of scripts.


# Networking fundamentals

## Fundamentals of internet protocols

Understand TCP/IP network fundamentals. Tasks include learning about
network masks; knowing the differences between private and public dotted
decimal IP addresses; setting a default route; understanding common TCP
and UDP ports (20, 21, 22, 23, 25, 53, 80, 110, 119, 139, 143, 161, 443,
465, 993, and 995); understanding the differences and major features of
UDP, TCP, and ICMP; and knowing the major differences between IPv4 and
IPv6.

* servers, sockets
* ping, traceroute

## Basic network configuration

View, change, and verify configuration settings on client hosts. Tasks
include manually and automatically configuring network interfaces and
configuring TCP/IP hosts.

* udev, systemd-networkd
* /etc/networking/interfaces
* /proc/net/dev
* ifconfig
* WPA Supplicant - /etc/wpa_supplicant/wpa_supplicant.conf
* iwconfig, iwlist
* watch 'iwconfig ; ifconfig'

Example: giving a USB-to-Ethernet (or Wifi) dongle a specific name and a
static IP address on hotplug

## Basic network troubleshooting

Troubleshoot networking issues on client hosts. Tasks include manually
and automatically configuring network interfaces and routing tables to
add, start, stop, restart, delete, or reconfigure network interfaces;
changing, viewing, or configuring the routing table; correcting an
improperly set default route manually; and debugging problems associated
with the network configuration.

* ping, traceroute
* route
* netstat, tcpdump
* netcat

**Examples**

  * netstat -anp | grep LIST
  * tcpdump -i eth0 -n not port 22

## Configure client side DNS

Configure DNS on a client host. Tasks include using DNS on the local
system and modifying the order in which name resolution is done.

* /etc/nsswitch.conf
* /etc/resolv.conf
* /etc/systemd/network/
* Avahi, Bonjour, Link-local addressing
* nslookup, whois


# Security

## Perform security administration tasks

* Review system configuration to ensure host security in accordance with
local security policies
* Find files with the suid/sgid bit set
* Setting or changing user passwords and password aging information
* netstat to discover open ports on a system
* setting limits on user logins
* basic sudo configuration and usage

## Firewalls

* iptables, ebtables
* shadow passwords and how they work
* stopping network services not in use
* TCP wrappers

## OpenSSH

* ~/.ssh/config
* Private/Public Key - authorized_keys
* port forwarding and tunneling
* SOCKS proxy
* X11 tunnels

## Securing data with encryption

* symmetric vs. assymetric encryption
* key management
* encrypted filesystems
* GnuPG


# Useful Examples

    du -sh /data/media/pictures/* /data/os/virtualbox/* | grep "^[0-9]*G"

## ADB incremental sync

Base command:

    adb shell ls -al /mnt/sdcard/DCIM/Camera/

    adb pull /mnt/sdcard/DCIM/Camera/<some image>.JPG ./

Automated recursion:

    adb shell 'ls /mnt/sdcard/DCIM/Camera/' \
    | tr -d 'r' \
    | while read f ; do
        b="$(basename "$f")"
        [[ ! -f "$b" ]] \
        && echo adb pull /mnt/sdcard/DCIM/Camera/$f ./
    done 2>&1 \
    | tail -n 2


# Advanced Topics

Copy-pasted from previous sections.  These topics are probably not
necessary, but included in the chance that there is interest

    xargs

## Use basic SQL commands

Query databases and manipulate data using basic SQL commands, including
joins or subselects. Tasks include using basic SQL commands and
performing basic data manipulation.

## Install a boot manager

Select, install, and configure a boot manager. Tasks include providing
alternative boot locations and backup boot options, installing and
configuring a boot loader such as GRUB or GRUB 2, and interacting with
the boot loader.

## Manage shared libraries

Determine the shared libraries that executable programs depend on, and
install them when necessary. Tasks include identifying shared libraries,
knowing the typical locations of system libraries, and loading shared
libraries.

## Process execution priorities

Manage process execution priorities. Tasks include determining the
default priority of jobs, running a program with higher or lower
priority, determining the priority of a process, and changing the
priority of a running process.

## Manage disk quotas

[bryant]: I haven't tried this in over a decade - I'll need to refresh my
knowledge if this is an interesting topic

Manage disk quotas for users. Tasks include setting up a disk quota for
a filesystem and editing, checking, and generating user quota reports.


# References

## LPIC-1: Linux Server Professional Certification

This series of tutorials helps you learn Linux system administration
tasks. The topics mirror those of the Linux Professional Institute's
<http://www.lpi.org/> LPIC-1: Linux Server Professional Certification
exams. You can use the tutorials to prepare for certification, or you
can use them to learn about Linux.

The material in these tutorials corresponds to version 4.0 of the
objectives of the LPIC-1 exams as updated April 15th, 2015 for LPIC-1

LPIC-1 exams:

* 101 <http://www.lpi.org/study-resources/lpic-1-101-exam-objectives/>
* 102 <http://www.lpi.org/study-resources/lpic-1-102-exam-objectives/>


## IBM developerWorks

A subset of developerWorks links included in the original document

* YouTube (https://www.youtube.com/user/developerworks)
* Tutorials & training (https://www.ibm.com/developerworks/learn/)
* Demos & sample code (https://developer.ibm.com/accelerate/)
* Q&A forums (https://developer.ibm.com/answers)
* dW Blog (https://developer.ibm.com/dwblog/)
* Events (https://developer.ibm.com/events/)
* Courses (https://developer.ibm.com/courses/)
* Learn (https://www.ibm.com/developerworks/learn/)
* Linux (https://www.ibm.com/developerworks/linux/)

## Miscellaneous

* [Advanced Bash Scripting Guide](http://tldp.org/LDP/abs/html/)

* [HP Proliant DL385 Produkt](https://www.hpe.com/at/de/product-catalog/servers/proliant-servers/pip.hpe-proliant-dl385-gen10-server.1010268408.html)
