
PDF_OUT = Linux_101_minimal_presentation.pdf
PDF_PAGE_OUT = Linux_101_minimal_page.pdf
TITLE = "Linux 101"
.PHONY: FORCE

#default: Linux_101.pdf Linux_101_minimal.pdf
default: $(PDF_OUT) $(PDF_PAGE_OUT)

$(PDF_OUT): Linux_101_minimal.md FORCE
#	pandoc -s -T "Linux 101" --from markdown_strict+footnotes+pandoc_title_block --toc --template=default.latex $< -o $@
	pandoc \
		--standalone \
		--title-prefix="Linux 101 Presentation" \
		-t beamer \
		--variable fontsize=10pt \
		--variable theme:CambridgeUS \
		--toc \
		--from markdown_strict+footnotes+pandoc_title_block \
		$< \
		-o $@
		

$(PDF_PAGE_OUT): Linux_101_minimal_page.md FORCE
#	pandoc -s -T "Linux 101" --from markdown_strict+footnotes+pandoc_title_block --toc --template=default.latex $< -o $@
	pandoc \
		--standalone \
		--title-prefix="Linux 101 Pages" \
		--template=default.latex \
		--toc \
		--from markdown_strict+footnotes+pandoc_title_block \
		$< \
		-o $@
		

Linux_101_minimal_page.md: Linux_101_minimal.md FORCE
	cat $< | grep -v '^----$$' > $@

themetest:
	/bin/bash ./make_all_themes.sh
