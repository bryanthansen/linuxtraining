% Linux 101 - an Audience-specific Tutorial
% Author Bryant Hansen docs@bryanthansen.net
% May 26, 2018


# Introduction

This content currently represents the merging of both original content with
blocks of copy-pasted content from IBM developerWorks guides
[Learn Linux, 101](https://www.ibm.com/developerworks/library/?series_title_by=learn+linux,+101).

Editing and further customizations are currently ongoing.

The current target audience has some of the following requirements:

* Maintenance-focused, as opposed to deployment-focused or development-focused
* No desktop GUI/environment
* Based on Ubuntu Server

This document has been converted to PDF
via [pandoc](https://en.wikipedia.org/wiki/Pandoc)
with the following command:

    pandoc --template=default.latex Linux_101.md -o Linux_101.pdf

It is maintained under source code control
via [git](https://en.wikipedia.org/wiki/Git)


## License

Some of the content in this repository falls under other licenses.

All externally-sourced content is properly-referenced.

Any original content can be considered licensed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)


## TODO

Organize the content better

  - referenced documents to the refs subfolder
  - images to an images subfolder
