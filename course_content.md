# Introduction to Linux

Bryant Hansen


## System Resources [ block diagram ]

* cpu - ps, top, htop, /proc/cpuinfo
* RAM - /proc/meminfo, free, swapon,
* drive - hdparm, du, df
* pci bus - lspci
* usb bus - lsusb

## System Components

* kernel, modules, lsmod, modinfo, modprobe, make menuconfig, make oldconfig,
* device nodes
* files & directories
  * permissions, umask,
* processes

## Networking

* servers, sockets, netstat
* tcpdump
* Examples:
  * netstat -anp | grep LIST

## An Introduction to Filesystems

* inodes & links
* blocks & block size
* comparison of filesystems (wikipedia)
* best choices: ext4, zfs, xfs, fat32
* cifs, tmpfs
* tools: /proc/filesystems, e2fsck, mkfs

## The Bash Shell

### Examples

*  du -sh /data/media/pictures/* /data/os/virtualbox/* | grep "^[0-9]*G"
*  background commands, &, fg, bg, ctrl-Z, nohup
*  log monitoring: tail -f
*  alias

## Core Utils

## Drivers (kernel modules)

*  lsmod, modinfo, modprobe

## Operations

## Advanced

*  xargs

## Higher-complexity 1-line Shell-script Examples:

### ADB incremental sync

Base command:

adb shell ls -al /mnt/sdcard/DCIM/Camera/
adb pull /mnt/sdcard/DCIM/Camera/<some image>.JPG ./

Automated recursion:

adb shell 'ls /mnt/sdcard/DCIM/Camera/' | tr -d 'r' | while read f ; do b="$(basename "$f")" ; [[ ! -f "$b" ]] && echo adb pull /mnt/sdcard/DCIM/Camera
/$f ./ ; done 2>&1 | tail -n 2

