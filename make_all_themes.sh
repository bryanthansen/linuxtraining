#!/bin/bash
# Bryant Hansen

THEME_DIR=/usr/share/texmf-site/tex/latex/beamer/base/themes/theme
IN=Linux_101_minimal.md
OUTDIR=tmp

[[ -d "$OUTDIR" ]] || mkdir -p "$OUTDIR"
rm -f "$OUTDIR"/Linux_101*.pdf
ls -1 "$THEME_DIR"/*.sty \
| head -n 2 \
| while read f ; do
    b="$(basename "${f//beamertheme/}")"
    n="${b%.sty}"
    out="${OUTDIR}/Linux_101_minimal_${n}.pdf"
    echo "generating $n (out = "$out") ..." >&2
    time pandoc \
        --standalone \
        --title-prefix="Linux 101 - $n Theme" \
        --variable theme:${n} \
        -t beamer \
        --variable fontsize=10pt \
        --toc \
        --from markdown_strict+footnotes+pandoc_title_block \
        "$IN" \
        -o "$out"
done

